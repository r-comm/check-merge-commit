#!/bin/bash
set -x 

if git show --summary HEAD | grep -q ^Merge: ; 
	then
		echo "Is a merge commit"; 
		echo "Perform a list of task such as upload artifact"
	else 
		echo "Not merge commit"; 
		echo "Exit script"
		exit 0; 
	fi